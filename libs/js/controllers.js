

	var app = angular.module('randomizer', ['ngMaterial'])

	app.controller('worldCupController', function($scope, $mdDialog) {

	    $mdDialog.show(
 			 $mdDialog.alert()
	        // .parent(angular.element(document.querySelector('#popupContainer')))
	        .clickOutsideToClose(true)
	        .title('Welcome to FIFA Tournament Administration.')
	        .textContent('Pick a random team or country for FIFA Tournament. Good Luck!')
	        // .ariaLabel('Alert Dialog Demo')
	        .ok('Got it!')
	        // .targetEvent()
		);

		$scope.states = [	
			{name: "Argentina"},
			{name: "Austria"},
			{name: "Belgium"},
			{name: "Brazil"},
			{name: "Chile"},
			{name: "Colombia"},
			{name: "Costa Rica"},
			{name: "Cote d'Ivoire"},
			{name: "Croatia"},
			{name: "Czech Republic"},
			{name: "Denmark"},
			{name: "Ecuador"},
			{name: "England"},
			{name: "France"},
			{name: "Germany"},
			{name: "Ghana"},
			{name: "Greece"},
			{name: "Ireland"},
			{name: "Italy"},
			{name: "Japan"},
			{name: "Korea Republic"},
			{name: "Mexico"},
			{name: "Netherlands"},
			{name: "Norway"},
			{name: "Paraguay"},
			{name: "United States"},
			{name: "Poland"},
			{name: "Portugal"},
			{name: "Russia"},
			{name: "Scotland"},
			{name: "Serbia"},
			{name: "Spain"},
		]

		// $scope.sort = function() {

		// 	$scope.states.sort(function(a,b) {
		// 		if (a.name < b.name) {
		// 			return -1
		// 		} else if (a.name > b.name) {
		// 		    return 1;
		// 	    } else { 
		// 		    return 0;
		// 	    }
		// 	})
		// }


		// LOCAL STORAGE //////////////////////////////////////

		var WC = localStorage['WC']
		if (WC !== undefined) {
			$scope.states = JSON.parse(WC)
		}

		$scope.save = function(msg) {
			
			for(var i=0;i<$scope.states.length;i++) {
				if($scope.states[i].name == msg) {
					$scope.states[i].name = event.target.InnerText;
				}
			}
			

			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited countries be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {

				   //  	$scope.states.sort(function(a,b) {
					  //   	if (a.name < b.name) {
							// 	return -1
							// } else if (a.name > b.name) {
							//     return 1;
						 //    } else { 
							//     return 0;
						 //    }
					  //   })

				     	localStorage['WC'] = JSON.stringify($scope.states)

				     		// Go back to EDIT
					     	$('.edit').removeClass('md-warn')
							$('.edit').addClass('md-primary')
							$('.input1').css('display', 'none')
							$('.save').css('display', 'none')
							$('.country').css('display', 'inline-block')
							$('.luck').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });
		}

		// LOCAL STORAGE //////////////////////////////////////


		$scope.editovanje = 'Edit'

		$scope.randomWorldState = function() {
			var randomNum = Math.random()*32
			var okruglo = Math.ceil(randomNum)

			$scope.randomNum = okruglo
		}


		$scope.luck = function() {

			var randomNum = Math.random()*32
			var okruglo = Math.floor(randomNum)+1

			$scope.random = $scope.states[okruglo - 1]

				$mdDialog.show(
		 			 $mdDialog.alert()
			        .parent(angular.element(document.querySelector('#worldCupAlert')))
			        .clickOutsideToClose(true)
			        // .title('Welcome to FIFA MZP Tournament Administration')                                               
			        .textContent(okruglo + ". " + $scope.random.name)
			        // .ariaLabel('Alert Dialog Demo')
			        .ok('Got it!')
			        .openFrom({
					          top: -50,
					          left: 1000,
					          width: 30,
					          height: 80
					        })
			        .closeTo({
					          left: 1500
		        	})
		        // .targetEvent()
			);
		}

		var active = true

		$scope.edit = function() {

			if (active) {
				$('.input1').css('display', 'inline-block')
				$('.save').css('display', 'inline-block')
				$('.country').css('display', 'none')
				$('.luck').css('display', 'none')
				$scope.editovanje = "X"
				active = false
				$('.edit').removeClass('md-primary')
				$('.edit').addClass('md-warn')
			} else {
				$('.edit').removeClass('md-warn')
				$('.edit').addClass('md-primary')
				$('.input1').css('display', 'none')
				$('.save').css('display', 'none')
				$('.country').css('display', 'inline-block')
				$('.luck').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}

		}

		$scope.randomNumWStates = function() {
			$scope.states.sort(function() {
				return Math.random()*32-5
			})
		}

		// Hide Save
			$('.saveBtn').css('display', 'none')
		// Hide Save
	})

	app.controller('euroCupController', function($scope, $mdDialog) {
		$scope.states = [	
			{name: "Austria"},
			{name: "Belgium"},
			{name: "Croatia"},
			{name: "Czech Republic"},
			{name: "Denmark"},
			{name: "England"},
			{name: "France"},
			{name: "Germany"},
			{name: "Greece"},
			{name: "Ireland"},
			{name: "Italy"},
			{name: "Netherlands"},
			{name: "Poland"},
			{name: "Portugal"},
			{name: "Serbia"},
			{name: "Spain"}
		]
		

		var EC = localStorage['EC']

		if (EC !== undefined) {
			$scope.states = JSON.parse(EC)
		}

		$scope.save = function(msg) {
			
			for(var i=0;i<$scope.states.length;i++) {
				if($scope.states[i].name == msg) {
					$scope.states[i].name = event.target.InnerText;
				}
			}

			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited countries be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {
				     	localStorage['EC'] = JSON.stringify($scope.states)

				     		// Go back to EDIT
					     	$('.edit2').removeClass('md-warn')
							$('.edit2').addClass('md-primary')
							$('.input2').css('display', 'none')
							$('.save2').css('display', 'none')
							$('.country').css('display', 'inline-block')
							$('.luck').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });
		}

		$scope.editovanje = 'Edit'
		var active = true


		$scope.edit = function() {

			if (active) {
				$('.input2').css('display', 'inline-block')
				$('.save2').css('display', 'inline-block')
				$('.country2').css('display', 'none')
				$('.luck2').css('display', 'none')
				$scope.editovanje = "X"
				active = false
				$('.edit2').removeClass('md-primary')
				$('.edit2').addClass('md-warn')
			} else {
				$('.edit2').removeClass('md-warn')
				$('.edit2').addClass('md-primary')
				$('.input2').css('display', 'none')
				$('.save2').css('display', 'none')
				$('.country2').css('display', 'inline-block')
				$('.luck2').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}
		}

		$scope.luck = function() {

			var randomNum = Math.random()*16
			var okruglo = Math.floor(randomNum)+1


			$scope.random = $scope.states[okruglo - 1]
				$mdDialog.show(
	 			 $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#euroCupAlert')))
		        .clickOutsideToClose(true)
		        // .title('Welcome to FIFA MZP Tournament Administration')
		        .textContent(okruglo + ". " + $scope.random.name)
		        // .ariaLabel('Alert Dialog Demo')
		        .ok('Got it!')
		        .openFrom({
				          top: -50,
				          left: 1000,
				          width: 30,
				          height: 80
				        })
		        .closeTo({
				          left: -1500
	        	})
		        // .targetEvent()
			);
		}

		$scope.randomEuroState = function() {
			var randomNum = Math.random()*16
			var okruglo = Math.ceil(randomNum)

			$scope.randomNum = okruglo
		}


		$scope.randomNumEStates = function() {
			$scope.states.sort(function() {
				return Math.random()*16-5
			})
		}
	})

	app.controller('uefaChampController', function($scope, $mdDialog) {
			$scope.teams = [	
			{name: "Borussia Dortmund"},
			{name: "Borussia M'gladbach"},
			{name: "FC Bayern Munich"},
			{name: "FC Schalke 04"},
			{name: "Vfl Wolfsburg"},
			{name: "AC Milan"},
			{name: "ACF Fiorentina"},
			{name: "AS Roma"},
			{name: "FC Inter"},
			{name: "Olympique Lyonnais"},
			{name: "Juventus FC"},
			{name: "SS Lazio"},
			{name: "SSC Napoli"},
			{name: "FC Porto"},
			{name: "Sl Benfica"},
			{name: "Zenit St. Petersburg"},
			{name: "Athletic Club Bilbao"},
			{name: "Athletico de Madrid"},
			{name: "FC Barcelona"},
			{name: "Real Madrid CF"},
			{name: "Real Sociedad Futbol"},
			{name: "Sevilla FC"},
			{name: "Valencia CF"},
			{name: "Arsenal FC"},
			{name: "Chelsea FC"},
			{name: "Everton FC"},
			{name: "Liverpool FC"},
			{name: "Manchester City FC"},
			{name: "Manchester United"},
			{name: "Tottenham Hotspur"},
			{name: "PSG"},
			{name: "PSV"}
		]


		var UCL = localStorage['UCL']

		if (UCL !== undefined) {
			$scope.teams = JSON.parse(UCL)
		}

		$scope.save = function(msg) {
			for(var i = 0; i<$scope.teams.length;i++) {
				if ($scope.teams[i].name == msg) {
					$scope.teams[i].name = event.target.InnerText
				}
			}

			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited teams be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {
				     	localStorage['UCL'] = JSON.stringify($scope.teams)

				     		// Go back to EDIT
					     	$('.edit3').removeClass('md-warn')
							$('.edit3').addClass('md-primary')
							$('.input3').css('display', 'none')
							$('.save3').css('display', 'none')
							$('.country').css('display', 'inline-block')
							$('.luck').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });

			
		}

		$scope.editovanje = 'Edit'
		var active = true
		

		$scope.edit = function() {

			if (active) {
				$('.input3').css('display', 'inline-block')
				$('.save3').css('display', 'inline-block')
				$('.team3').css('display', 'none')
				$('.luck3').css('display', 'none')
				$scope.editovanje = "X"
				active = false
				$('.edit3').removeClass('md-primary')
				$('.edit3').addClass('md-warn')
			} else {
				$('.edit3').removeClass('md-warn')
				$('.edit3').addClass('md-primary')
				$('.input3').css('display', 'none')
				$('.save3').css('display', 'none')
				$('.team3').css('display', 'inline-block')
				$('.luck3').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}
		}

		$scope.randomUefaChampionsTeamsNumber = function() {
			var randomNum = Math.random()*32
			var okruglo = Math.ceil(randomNum)

			$scope.randomNum = okruglo
		}

		$scope.luck = function() {

			var randomNum = Math.random()*32
			var okruglo = Math.floor(randomNum)+1

			$scope.random = $scope.teams[okruglo - 1]

				$mdDialog.show(
	 			 $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#uefaChampAlert')))
		        .clickOutsideToClose(true)
		        // .title('Welcome to FIFA MZP Tournament Administration')
		        .textContent(okruglo + ". " + $scope.random.name)
		        // .ariaLabel('Alert Dialog Demo')
		        .ok('Got it!')
		        // You can specify either sting with query selector
				.openFrom({
				          top: -50,
				          left: 1000,
				          width: 30,
				          height: 80
				        })
				        .closeTo({
				          left: 1500
		        	})
				);
			}

		$scope.randomUefaChampionsTeams = function() {
			$scope.teams.sort(function() {
				return Math.random()*32-5
			})
		}
	})



	app.controller('euroChampController', function($scope, $mdDialog) {
			$scope.teams = [	
			{name: "Hertha BSC"},
			{name: "Southampton"},
			{name: "Stoke"},
			{name: "West Ham"},
			{name: "Lille"},
			{name: "St Etienne"},
			{name: "Udinese"},
			{name: "Torino"},
			{name: "Ajax"},
			{name: "Sporting"},
			{name: "FC Shakhtar"},
			{name: "Lokomotiv"},
			{name: "CSKA"},
			{name: "Olympiacos F.C."},
			{name: "Malaga"},
			{name: "Villareal"},
			{name: "Celta Vigo"},
			{name: "Real Sociedad"},
			{name: "Besiktas "},
			{name: "Fenerbahce"},
			{name: "Galatasaray"},
			{name: "Spartak "},
			{name: "Bordeaux "},
			{name: "Hamburg"},
			{name: ""},
			{name: ""},
			{name: ""},
			{name: ""},
			{name: ""},
			{name: ""},
			{name: ""},
			{name: ""}
		]

		var UEL = localStorage['UEL']

		if (UEL !== undefined) {
			$scope.teams = JSON.parse(UEL)
		}

		$scope.save = function(msg) {
			for(var i = 0; i<$scope.teams.length;i++) {
				if ($scope.teams[i].name == msg) {
					$scope.teams[i].name = event.target.InnerText
				}
			}

			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited teams be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {
				     	localStorage['UEL'] = JSON.stringify($scope.teams)

				     		// Go back to EDIT
					     	$('.edit4').removeClass('md-warn')
							$('.edit4').addClass('md-primary')
							$('.input4').css('display', 'none')
							$('.save4').css('display', 'none')
							$('.country').css('display', 'inline-block')
							$('.luck').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });

			
		}


		$scope.editovanje = 'Edit'
		var active = true


		$scope.edit = function() {

			if (active) {
				$('.input4').css('display', 'inline-block')
				$('.save4').css('display', 'inline-block')
				$('.team4').css('display', 'none')
				$('.luck4').css('display', 'none')
				$scope.editovanje = "X"
				active = false
				$('.edit4').removeClass('md-primary')
				$('.edit4').addClass('md-warn')
			} else {
				$('.edit4').removeClass('md-warn')
				$('.edit4').addClass('md-primary')
				$('.input4').css('display', 'none')
				$('.save4').css('display', 'none')
				$('.team4').css('display', 'inline-block')
				$('.luck4').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}
		}


		$scope.randomUefaChampionsTeamsNumber = function() {
			var randomNum = Math.random()*32
			var okruglo = Math.ceil(randomNum)

			$scope.randomNum = okruglo
		}

		$scope.luck = function() {

			var randomNum = Math.random()*32
			var okruglo = Math.floor(randomNum)+1

			$scope.random = $scope.teams[okruglo - 1]

			$mdDialog.show(
	 			 $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#uefaEuroAlert')))
		        .clickOutsideToClose(true)
		        // .title('Welcome to FIFA MZP Tournament Administration')
		        .textContent(okruglo + ". " + $scope.random.name)
		        // .ariaLabel('Alert Dialog Demo')
		        .ok('Got it!')
				.openFrom({
			          top: -50,
			          left: 1000,
			          width: 30,
			          height: 80
			        })
	        	.closeTo({
			          left: 1500
	        	})

		        // .targetEvent()
			);
		}

		$scope.randomUefaChampionsTeams = function() {
			$scope.teams.sort(function() {
				return Math.random()*32-5
			})
		}

	})



	app.controller('customCtrl', function($scope,$mdDialog) {
		var active = true

		$('.burger').click(function() {
			if (active) {
				var delay = 0
				delay += 0.1
				$('.all').css('display','none')
				$('.white').css('position','static')
				$('.white').css('display','block')
				TweenLite.set($('.white'), {scale: 0})
				TweenLite.set($('.white'), {top: '0%'})
				TweenLite.to($('.white'), 1, {scale:1 ,ease: Power4.easeOut, delay:delay})
				TweenLite.to($('.burger'), 1, {top: "-=14%"})
				TweenLite.to($('.burger2'), 0.5, {alpha:0, delay:delay})
				TweenLite.to($('.burger1'), 0.5, {rotation:45, delay:delay})
				TweenLite.to($('.burger3'), 0.5, {css:{marginTop:-9}, delay:delay})
				TweenLite.to($('.burger3'), 0.5, {rotation:-45, delay:delay})
				// TweenLite.to($('.burger1'), 0.5, {css:{background: "#FF5722"}})
				// TweenLite.to($('.burger2'), 0.5, {css:{background: "#FF5722"}})
				// TweenLite.to($('.burger3'), 0.5, {css:{background: "#FF5722"}})
				TweenLite.to($('.burger'), 0.5, {css:{background: "#FF5722"}})
				active = false
			} else {
				$('.all').css('display','block')
				$('.white').css('position','fixed')
				TweenLite.to($('.burger'), 1, {top: "+=14%"})
				TweenLite.to($('.burger2'), 0.5, {alpha:1})
				TweenLite.to($('.burger1'), 0.5, {rotation:0})
				TweenLite.to($('.burger3'), 0.5, {css:{marginTop:3}})
				TweenLite.to($('.burger3'), 0.5, {rotation:0})
				var delay = 0;
				TweenLite.to($('.white'), 0.5, {scale: 0, top: '+80%', ease: Linear.easeNone})
				delay += 0.6
				TweenLite.set($('.white'), {display:"none", delay: delay})
				// TweenLite.to($('.burger1'), 0.5, {css:{background: "#EEEEEE"}, delay: twnDelay})
				// TweenLite.to($('.burger2'), 0.5, {css:{background: "#EEEEEE"}, delay: twnDelay})
				// TweenLite.to($('.burger3'), 0.5, {css:{background: "#EEEEEE"}, delay: twnDelay})
				TweenLite.to($('.burger'), 0.5, {css:{background: "#3F51B5"}})
				active = true
			}
		})


	})

	app.controller('custom16Ctrl', function($scope, $mdDialog) {
		$scope.t16 = [
			{name: "custom1"},
			{name: "custom2"},
			{name: "custom3"},
			{name: "custom4"},
			{name: "custom5"},
			{name: "custom6"},
			{name: "custom7"},
			{name: "custom8"},
			{name: "custom9"},
			{name: "custom10"},
			{name: "custom11"},
			{name: "custom12"},
			{name: "custom13"},
			{name: "custom14"},
			{name: "custom15"},
			{name: "custom16"},
		]


		var C2 = localStorage['C2']

		if (C2 !== undefined) {
			$scope.t16 = JSON.parse(C2)
		}

		$scope.tourName = JSON.parse(localStorage.getItem('C2Name'))


		$scope.save = function(msg) {
			for(var i = 0; i<$scope.t16.length;i++) {
				if ($scope.t16[i].name == msg) {
					$scope.t16[i].name = event.target.InnerText
				}
			}

			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited teams/countries be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {
				     	localStorage['C2'] = JSON.stringify($scope.t16)
				     	localStorage['C2Name'] = JSON.stringify($scope.tourName)

				     		// Go back to EDIT
					     	$('.edit5').removeClass('md-warn')
							$('.edit5').addClass('md-primary')
							$('.input5').css('display', 'none')
							$('.input8').css('display', 'inline-block')
							$('.save5').css('display', 'none')
							$('.country5').css('display', 'inline-block')
							$('.luck5').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });

			
		}


		$scope.editovanje = "edit"

		var active = true

		$scope.edit = function() {
			
			if (active) {
				$('.input5').css('display', 'inline-block')
				$('.input8').css('display', 'none')
				$('.save5').css('display', 'inline-block')
				$('.team5').css('display', 'none')
				$('.luck5').css('display', 'none')
				$('.country5').css('display', 'none')
				$scope.editovanje = "X"
				$('.edit5').removeClass('md-primary')
				$('.edit5').addClass('md-warn')
				active = false
			} else {
				$('.edit5').removeClass('md-warn')
				$('.edit5').addClass('md-primary')
				$('.input5').css('display', 'none')
				$('.save5').css('display', 'none')
				$('.team5').css('display', 'inline-block')
				$('.luck5').css('display', 'inline-block')
				$('.input8').css('display', 'inline-block')
				$('.country5').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}
		}

		// $scope.customNumPrint = function() {
		// 	var randomNum = Math.random()*16
		// 	var okruglo = Math.ceil(randomNum)

		// 	$scope.randomNum = okruglo
		// }

		$scope.luck = function() {

			var randomNum = Math.random()*16
			var okruglo = Math.floor(randomNum)+1

			$scope.random = $scope.t16[okruglo - 1]

			$mdDialog.show(
	 			 $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#custom1')))
		        .clickOutsideToClose(true)
		        // .title('Welcome to FIFA MZP Tournament Administration')
		        .textContent(okruglo + ". " + $scope.random.name)
		        // .ariaLabel('Alert Dialog Demo')
		        .ok('Got it!')
				.openFrom({
			          top: -50,
			          left: 1000,
			          width: 30,
			          height: 80
			        })
	        	.closeTo({
			          left: 1500
	        	})

		        // .targetEvent()
			);
		}

		$scope.custom1Random = function() {
			$scope.t16.sort(function() {
				return Math.random()*16-5
			})
		}

	})

	app.controller('custom8Ctrl', function($scope, $mdDialog) {
		
		$scope.t8 = [
			{name: "custom1"},
			{name: "custom2"},
			{name: "custom3"},
			{name: "custom4"},
			{name: "custom5"},
			{name: "custom6"},
			{name: "custom7"},
			{name: "custom8"}
		]

		var C1 = localStorage['C1']

		if (C1 !== undefined) {
			$scope.t8 = JSON.parse(C1)
		}

		$scope.tourName = JSON.parse(localStorage.getItem('C1Name'))


		$scope.save = function(msg, name) {
			for(var i = 0; i<$scope.t8.length;i++) {
				if ($scope.t8[i].name == msg) {
					$scope.t8[i].name = event.target.InnerText
				}
			}


			var confirm = $mdDialog.confirm()
       					  .parent(angular.element(document.querySelector('#saveConfirm')))
				          // .title("Don't save if you don't want to edited teams/countries be here after reloading page")
				          .textContent('Are you sure you want to save ?')
			       		  .clickOutsideToClose(true)
				          // .ariaLabel('Lucky day')
				          .targetEvent()
				          .ok('Yes')
				          .cancel('No');

				    $mdDialog.show(confirm).then(function() {
				     	localStorage['C1'] = JSON.stringify($scope.t8)
		     			localStorage['C1Name'] = JSON.stringify($scope.tourName)

				     		// Go back to EDIT
					     	$('.edit6').removeClass('md-warn')
							$('.edit6').addClass('md-primary')
							$('.input6').css('display', 'none')
							$('.input7').css('display', 'inline-block')
							$('.save6').css('display', 'none')
							$('.country6').css('display', 'inline-block')
							$('.luck6').css('display', 'inline-block')
							$scope.editovanje = "Edit"
							active = true
				     		// Go back to EDIT

				    }, function() {
				      
				    });

			
		}




		$scope.editovanje = "edit"

		var active = true

		$scope.edit = function() {
			
			if (active) {
				$('.input6').css('display', 'inline-block')
				$('.input7').css('display', 'none')
				$('.save6').css('display', 'inline-block')
				$('.team6').css('display', 'none')
				$('.luck6').css('display', 'none')
				$('.country6').css('display', 'none')
				$scope.editovanje = "X"
				$('.edit6').removeClass('md-primary')
				$('.edit6').addClass('md-warn')
				active = false
			} else {
				$('.edit6').removeClass('md-warn')
				$('.edit6').addClass('md-primary')
				$('.input6').css('display', 'none')
				$('.save6').css('display', 'none')
				$('.team6').css('display', 'inline-block')
				$('.luck6').css('display', 'inline-block')
				$('.input7').css('display', 'inline-block')
				$('.country6').css('display', 'inline-block')
				$scope.editovanje = "Edit"
				active = true
			}
		}

		// $scope.customNumPrint = function() {
		// 	var randomNum = Math.random()*16
		// 	var okruglo = Math.ceil(randomNum)

		// 	$scope.randomNum = okruglo
		// }

		$scope.luck = function() {

			var randomNum = Math.random()*8
			var okruglo = Math.floor(randomNum)+1

			$scope.random = $scope.t8[okruglo - 1]

			$mdDialog.show(
	 			 $mdDialog.alert()
		        .parent(angular.element(document.querySelector('#custom1')))
		        .clickOutsideToClose(true)
		        // .title('Welcome to FIFA MZP Tournament Administration')
		        .textContent(okruglo + ". " + $scope.random.name)
		        // .ariaLabel('Alert Dialog Demo')
		        .ok('Got it!')
				.openFrom({
			          top: -50,
			          left: 1000,
			          width: 30,
			          height: 80
			        })
	        	.closeTo({
			          left: 1500
	        	})

		        // .targetEvent()
			);
		}

		$scope.custom2Random = function() {
			$scope.t8.sort(function() {
				return Math.random()*8-5
			})
		}


	})